package org.nrg.xnat.plugin;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;

@XnatPlugin(value = "cnda_plugin_cndaplugindependencies", name = "CNDA 1.7 Dependencies Plugin", description = "This is the CNDA 1.7 Dependencies Plugin.",
        dataModels = {
		})
public class CndaPluginDependenciesPlugin {
}